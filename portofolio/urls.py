from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.portofolio, name='portofolio'),
    path('form', views.form, name='form'),
]
