SHELL = /bin/bash
.PHONY: deployment

deployment:
	python3 -m venv env
	source env/bin/activate
	python3 -m pip install -r requirements.txt
	find . -path "*/migrations/*.pyc" -delete
	python manage.py collectstatic
	python manage.py makemigrations
	python manage.py migrate
	python manage.py runserver

